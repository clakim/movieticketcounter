# Box Office Ticket Agent Mock Application

Customer Functionality:  
-Buy tickets for a movie up to 7 days in advance  
-Get a refund for a ticket whose movie has not yet been shown

Agent Functionality:  
-Sell tickets for any movie for an upcoming showtime  
-Refund a ticket when presented at the counter, if the movie has not started yet  
-Generate a report of the number of tickets sold and number of vacant seats for any given showtime, past or future  
-Generate a report of the total number of tickets sold on any given date


Specifications:  
1. Main menu ("portal") lets user act as an agent or viewer or both.  
2. If user takes on Viewer (essentially, ticket-purchaser/ticket-refund-requester) role, it cannot assume role of another Viewer while the program is running. It follows that if the agent wishes to generate one of the two types of reports, any data comes from all purchases made by this Viewer.  
3. Assume that 5 movies can be shown each theatre can show 5 screens at the same time. Assume that one unique showtime corresponds to a (movie,date,time) combination, and each showtime accommodates 200/5  = 40 seats.  
4. There are 40 seats each for 5 movies playing at matinee time, same for evening time.  
5. Assume all movies are shown year-round.  
6. Agent issue refund on the same day of the showtime of a purchased ticket, but cannot refund after this day.    
7. Implemented this pricing for the 4 ticket tiers:  
    - Tier 1:  Weekend Nights - $15  
    - Tier 2:  Weekends days - $12  
    - Tier 3:  Weekday Nights - $10  
    - Tier 4:  Weekday Days - $8  
8. For simplification, weekends are considered dates with days that are multiples of 5, 6, or 7.  
9. MovieID starts with 1  
10. Viewers can only buy one group of tickets at a time (group being defined by showtime (movie,date,time))  
11. If viewer wants a refund, viewer must request one per individual ticket, and viewer must know the ticket ID  
12. For simplicity's sake, the "number of tickets sold for a showtime" and "total number of tickets sold on a certain date" is not changed by refunds. Number of empty seats is dependent solely on number of tickets sold, regardless of refunds.  
13. While originally, completion of an interaction sequence (e.g. viewer purchasing ticket or viewer requesting refund) would return to the main menu, for testing purposes I created a dummyMenu method to simulate a return to the menu that waits for user input (otherwise a test would never reach an end).
