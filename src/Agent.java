import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Agent class
 */

public class Agent {

    // Determine how much money to refund based on ticket qualities
    public int sendMoney(Ticket ticket) {
        System.out.println("This is ticket cost: " + ticket.getCost());
        String now2 = new SimpleDateFormat("MMddyy").format(new Date());
        if ((Integer.parseInt(now2.substring(2, 4))) <= (Integer.parseInt(ticket.getDate().substring(2, 4)))) {

            return ticket.getCost();
        } else {
            System.out.println("Can't issue refund because date passed already");
            return 0;
        }
    }

    // Generate a report of the number of tickets sold AND number of vacant seats for any given showtime, past or future
    public int genShowtimeReport(int movieID, String date, int time) {

        int inCount = 0;
        int temp = inCount;

        for (int index = 0; index < Runner.groupedTicketsbyShowtime.keySet().toArray().length; index++) {
            if ((Runner.groupedTicketsbyShowtime.keySet().toArray()[index]).equals(Integer.toString(movieID) + date + Integer.toString(time))) {
                temp = Runner.groupedTicketsbyShowtime.get(Runner.groupedTicketsbyShowtime.keySet().toArray()[index]);
            }
        }

        if (temp >= 40) { temp = 40; }
        System.out.println("Number of tickets sold for given showtime (movie,date,time) = " + (temp + inCount));
        System.out.println("Number of empty seats for given showtime (movie,date,time) = " + (40 - (temp + inCount)));

        return  temp + inCount;
    }


    // Generate a report of the total number of tickets sold on any given date.
    public int genDateReport(String date){
        int count = 0;
        for (Ticket ticket:Runner.tickets)
        {
            if (ticket.getDate().equals(date))
            { count++; }
        }
        System.out.println("Tickets sold for date " + date + " = " + count);
        return count;
    }
}