import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;

public class AgentTest {

    Agent a;

    @Before
    public void setUp() throws Exception {
        System.out.println("Setting up ...");
        a = new Agent();
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Tearing down ...");
        a=null;
    }
//
//    @Test
//    public void testMonSend() {
//        Agent b = new Agent();
//        ticket = new Ticket(4,"042818",1);
//        assertEquals(b.sendMoney(ticket), 15);
//    }

//    @Test
//    public void testsendMoney() {
//        Agent b = new Agent();
//        ticket = new Ticket(4,"042918",1);
//        assertEquals(b.sendMoney(ticket), 15);
//    }


    @Test
    public void genShowtimeReport() throws IOException, SQLException {
        Agent b = new Agent();
        Viewer v = new Viewer();
        String date = "042918";
        v.instantiateTicket(2,"042918",1,3);
        v.instantiateTicket(2,"042918",1,7);
        v.instantiateTicket(2,"042918",1,10);
        v.instantiateTicket(2,"042918",1,10);
        v.instantiateTicket(1,"042918",1,2);
        assertEquals(b.genShowtimeReport(2,"042918",1),30);
    }


    @Test
    public void genDateReport() throws IOException, SQLException {
//        Agent b = new Agent();
//        Viewer v = new Viewer();
//        String date = "042918";
//        v.instantiateTicket(2,"042918",1,3);
//        v.instantiateTicket(2,"042918",1,7);
//        v.instantiateTicket(2,"042918",1,10);
//        v.instantiateTicket(2,"042918",1,10);
//        v.instantiateTicket(2,"042918",1,10);
//        v.instantiateTicket(3,"042918",1,10);
//        v.instantiateTicket(3,"042918",1,10);
//        v.instantiateTicket(3,"042918",1,10);
//        v.instantiateTicket(3,"042918",1,10);
//        v.instantiateTicket(4,"042918",1,10);
//        v.instantiateTicket(4,"042918",1,10);
//        v.instantiateTicket(4,"042918",1,10);
//        v.instantiateTicket(4,"042918",1,10);
//        v.instantiateTicket(5,"042918",1,10);
//        v.instantiateTicket(5,"042918",1,10);
//        v.instantiateTicket(5,"042918",1,10);
//        v.instantiateTicket(5,"042918",1,10);
//        v.instantiateTicket(1,"042918",1,10);
//        v.instantiateTicket(1,"042918",1,10);
//        v.instantiateTicket(1,"042918",1,10);
//        v.instantiateTicket(1,"042918",1,10);
//        v.instantiateTicket(1,"042918",1,10);
//        v.instantiateTicket(1,"042918",1,10);
//        v.instantiateTicket(1,"042918",0,10);
//
//        assertEquals(b.genDateReport(date),210);
    }



//    @Test
//    public void testSell() {
//        Viewer v = new Viewer();
//        ticket = new Ticket(4,"042818",1);
//        int b1 = v.balance;
//        Agent b = new Agent();
//        //b.sell(v); // no longer existing method
//        int b2 = v.balance;
////        assertEquals("Balance should have 15 subtracted", -15, b2-b1);
//        assertEquals("Balance should be 0", 0, b2-b1);
//

//        //balance should change
//    }

}