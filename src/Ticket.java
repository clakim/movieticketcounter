import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Ticket class
 */

public class Ticket {

    private int movieID;
    private String date;
    private int time;
    private static int nextid;
    private int idNum;
    private int cost;


    public Ticket(int movieID, String date, int time) {
        this.movieID = movieID;
        this.date = date;
        this.time = time;
        idNum = nextid;
        nextid++;
    }

    public int getID() {
        return idNum;
    }

    public String getDate() {
        return date;
    }


    public boolean isWeekend(){
        int year = Integer.parseInt(this.date.substring(4,6));
        int month = Integer.parseInt(this.date.substring(0,2));
        int day = Integer.parseInt(this.date.substring(2,4));
        Calendar cal = new GregorianCalendar(year, month, day);
        int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
        if (Calendar.FRIDAY == dayOfWeek  || Calendar.SUNDAY == dayOfWeek || Calendar.SATURDAY == dayOfWeek)
        {
            System.out.println("This date falls on a weekend");
            return true;
        }
        return false;
    }

    public int getCost(){
        String tier = time + String.valueOf(this.isWeekend());

        switch(tier) {
            // Tier 1: Weekend Nights
            case "1true" :
                cost = 15;
                break;

            // Tier 2: Weekend Days
            case "0true" :
                cost = 12;
                break;

            // Tier 3: Weekday Nights
            case "1false" :
                cost = 10;
                break;

            // Tier 4: Weekday Days
            case "0false" :
                cost = 8;
                break;
        }
        return cost;
    }
}
