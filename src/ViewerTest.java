import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;

public class ViewerTest {
    Runner runner;
    Viewer v;
    Ticket t;


    @Before
    public void setUp() throws Exception {
        //runner = new Runner();
        v = new Viewer();
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void mainBuyTicket() throws IOException, SQLException, ParseException {
        //v.requestTicket();
        v.instantiateTicket(2,"042918",1,3);
//        v.instantiateTicket(3,"050918",1,1);

//        v.instantiateTicket(2,"050118",0,1);
//        v.instantiateTicket(2,"050118",0,1);
//        v.instantiateTicket(2,"050118",0,10);
//        v.instantiateTicket(2,"050118",0,10);
//
//        v.instantiateTicket(3,"050118",0,10);
//        v.instantiateTicket(3,"050118",0,10);
//        v.instantiateTicket(3,"050118",0,10);
//        v.instantiateTicket(3,"050118",0,10);
//
//        v.instantiateTicket(4,"050118",0,10);
//        v.instantiateTicket(4,"050118",0,10);
//        v.instantiateTicket(4,"050118",0,10);
//        v.instantiateTicket(4,"050118",0,10);
//
//        v.instantiateTicket(5,"050118",0,10);
//        v.instantiateTicket(5,"050118",0,10);
//        v.instantiateTicket(5,"050118",0,10);
//        v.instantiateTicket(5,"050118",0,10);
//
//        v.instantiateTicket(5,"050118",0,10);
//        v.instantiateTicket(5,"050118",0,10);
//        assertEquals(Runner.tickets.size(),200);

        assertEquals(Runner.tickets.size(),3);
        assertEquals(Viewer.balance, -45);
        v.getRefund(2);
        assertEquals(Viewer.balance, -30);

        v.instantiateTicket(7,"050118",1,3);
        v.getRefund(2);
        v.getRefund(3);
        v.getRefund(4);

        assertEquals(Viewer.balance, -30);

    }
}
