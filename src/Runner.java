import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Scanner;

/*
 * Driver program demonstrates app's ability to process buys, refunds, and reports for a movie theater.
 */

public class Runner {

    static ArrayList<Ticket> tickets = new ArrayList<Ticket>();
    static Scanner in = new Scanner(System.in);
    //static Agent a;
    static LinkedHashMap<String, Integer> groupedTicketsbyShowtime = new LinkedHashMap<String, Integer>();    // An ordered hashmap of tickets by showtime group
    static String now = new SimpleDateFormat("MMddyy").format(new Date());


    // Launch the main menu
    public static void main(String[] args) throws IOException, SQLException {
        //a = new Agent();
        System.out.println("Today's date = " + now);
        menu();
    }

    // Main menu with options: (B)uy ticket, (R)equest refund, (I)ssue refund, (S)ell ticket, (G)enerate report
    public static void menu() throws IOException, SQLException {
        Viewer v = new Viewer();
        Agent a = new Agent();

        System.out.println("Press (B) for Buy Ticket,");
        System.out.println("      (R) to Request Refund for Ticket,");
        System.out.println("      (I) to Issue Refund for Ticket,");
        System.out.println("      (S) for Sell Ticket,");
        System.out.println("      (G) for Generate Report,");
        System.out.println("      (Q) to quit");
        System.exit(1);
        String input = in.next();


        // TICKET TRANSACTION: (b)uy or (s)ell
        if(input.equalsIgnoreCase("b") || input.equalsIgnoreCase("s")) {
                v.requestTicket();
        }


        // REFUND: request or issue
        else if(input.equalsIgnoreCase("r") || input.equalsIgnoreCase("i")) {
            v.specifyRefund();
        }


        // GENERATE report: by showtime or by date
        else if(input.equalsIgnoreCase("g")) {

            System.out.println("Enter 's' or 'd': report for (s)howtime or (d)ate?");
            String type = in.next();

            // generate record for # of tickets for (s)howtime
            if(type.equalsIgnoreCase("s")) {
                System.out.println("Give movie ID number");
                int sMovie = in.nextInt();
                System.out.println("Enter the date to generate the report on, mmddyy format (e.g. 041118): ");
                String sDate = in.next();
                System.out.println("Give time of movie: enter 0 if matinee, 1 if evening");
                int sTime = in.nextInt();
                a.genShowtimeReport(sMovie,sDate,sTime);
            }

            // generate record for # of tickets for (d)ate
            else if(type.equalsIgnoreCase("d"))
            {
                System.out.println("Enter the date to generate the report on, mmddyy format (e.g. 041118): ");
                String dDate = in.next();
                a.genDateReport(dDate);
            }
            menu();
        }

        else if(input.equalsIgnoreCase("q")) {
            System.out.println("Goodbye!");
            System.exit(1);
        }
        else {
            System.out.println("Invalid key input");
            menu();
        }

    }
}