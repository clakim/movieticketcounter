import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Iterator;

/**
 * Viewer class
 */

public class Viewer {

    static int balance;

    public void dummyMenu(){
        System.out.println("Mock Main menu: press B to modifyBalance tickets [...]");
    }


    // Ask for 1) which movie 2) which date 3) which time of day 4) how many tickets
    public void requestTicket() throws IOException, SQLException {

        System.out.println("Give movie ID number");
        int movie = Runner.in.nextInt();
        //int movie = 1;

        System.out.println("Give date of movie in this format: mmddyy, e.g. 041018");
        String date = Runner.in.next();

        //String date = "042518";

        System.out.println("Give time of movie: enter 0 if matinee, 1 if evening");
        int time = Runner.in.nextInt();
        //int time = 1;

        System.out.println("Give number of tickets to modifyBalance");
        int ticketCount = Runner.in.nextInt();
        //int ticketCount = 10;

        if (isValidDate(date) && isValidTime(time)){
            instantiateTicket(movie,date,time,ticketCount);
        }
    }

    public boolean isValidDate(String date){

        // Checks that date is formatted to 6 characters
        if (date.length() != 6) {
            System.out.println("Date should be 6 characters, mmddyy format");
            return false;
        }

        // Checks for number input for date
        for (int i = 0; i < date.length(); i++){
//            System.out.println(this.getDate().charAt(i));
            if (!Character.isDigit(date.charAt(i))){
                System.out.println("Characters must be numbers");
                return false;
            }
        }

        // Checks that date and month are entered in correct order
        if ((Integer.parseInt(date.substring(0,2)) > 12) || (Integer.parseInt(date.substring(2,4)) > 31)) {
            System.out.println("Months belong first, then date of month (mmdd)");
            return false;
        }

        // Checks for no less than 7 days before showtime
        if (java.time.temporal.ChronoUnit.DAYS.between( LocalDate.parse("20" + Runner.now.substring(4,6) + "-" + Runner.now.substring(0,2)
                + "-" + Runner.now.substring(2, 4)), LocalDate.parse("20" + date.substring(4,6) + "-" + date.substring(0,2)
                + "-" + date.substring(2, 4)))>7)
        {
            System.out.println("Sorry, trying to purchase too many days in advance");
            return false;
        }

        // Checks for valid date input for this month
        try{
            LocalDate.of(Integer.parseInt(date.substring(4,6)), Integer.parseInt(date.substring(0,2)), Integer.parseInt(date.substring(2,4)));
        }
        catch(Exception e){
            System.out.println("Not enough days in this month");
        }

        return true;
    }


    public boolean isValidTime(int time) {
        System.out.println("This is time: " + time);
        if (!(time == 0 || time == 1)) {
            System.out.println("Showtime must be 0 for matinee or 1 for evening");
            return false;
        }
        return true;
    }



    public void instantiateTicket(int movie, String date, int time, int ticketCount) throws IOException, SQLException {

        if (ticketCount >= 11) {
            System.out.println("Sorry, no more than 10 tickets per transaction");
            dummyMenu();
            return;
            //Runner.menu();
        }

        String key = Integer.toString(movie) + date + Integer.toString(time);
        int counterinit = 0;

        // add key for a showtime if not already there
        Runner.groupedTicketsbyShowtime.putIfAbsent(key, counterinit);


        // instantiates Ticket and adds to global list
        for (int i = 0; i < ticketCount; i++) {
            int k = Runner.groupedTicketsbyShowtime.get(key);
            k++;


            Runner.groupedTicketsbyShowtime.put(key, k);
            //System.out.println("This is groupedTicketByShowtime: " + Runner.groupedTicketsbyShowtime.toString());
            //System.out.println("This is Runner.allTicketsDistributed.get(key): " + Runner.groupedTicketsbyShowtime.get(key));
            if (Runner.groupedTicketsbyShowtime.get(key) >= 41) {
                System.out.println("No more seats left for this showtime");
                break;
            }

            Runner.tickets.add(new Ticket(movie, date, time));
            modifyBalance(new Ticket(movie, date, time));
        }

        //System.out.println("tickets.size() = " + Runner.tickets.size());


        dummyMenu();
    //        try {
    //            Runner.menu();
    //        } catch (IOException e) {
    //            e.printStackTrace();
    //        } catch (SQLException e) {
    //            e.printStackTrace();
    //        }
    }


    // Update balance according to ticket purchase
    public void modifyBalance(Ticket ticket){
        balance = balance - ticket.getCost();
        System.out.println("Balance is now: " + balance);
    }


    public void specifyRefund(){

        System.out.println("Give the ID of the ticket you wish to refund: e.g. 2");
        int tickID = Runner.in.nextInt();
        getRefund(tickID);
    }

    public void getRefund(int ticketID)
    {
        // User Iterator to ask for refund
        Iterator<Ticket> ite = Runner.tickets.iterator();
        while(ite.hasNext()) {
            Ticket ticket = ite.next();
            //System.out.println("ticket.getID()= " + ticket.getID());

            if (ticket.getID() == ticketID) {
                this.requestMoney(ticket);
                break;
            }
        }
        dummyMenu();
//        try {
//            Runner.menu();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
    }


    // Update balance after asking an agent to send money back
    public void requestMoney(Ticket ticket){
        Agent a1 = new Agent();
        System.out.println("Requesting money back for ticket: " + ticket.getID());

        a1.sendMoney(ticket);
        balance = balance + a1.sendMoney(ticket);
        System.out.println("Current balance: " + balance);
        dummyMenu();
    }
}


